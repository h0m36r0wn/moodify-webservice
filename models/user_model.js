var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userModel = new Schema({
    email:{
      type:String
    },
    first_name:{
      type:String
    },
    last_name:{
      type:String
    },
    gender:{
      type:String
    },
    password:{
      type:String
    },
    profile_pic:{
      type:String
    },
    current_mood:{
      type:String
    },
    is_new:{
      type:Boolean,
      default:true
    },
    current_track:{
      type:String
    }
});

module.exports = mongoose.model('Users',userModel);
