var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var preferenceModel = new Schema({
    preferences:{
      type:[String]
    },
    user_id:{
      type:Schema.Types.ObjectId
    }
});

module.exports = mongoose.model('Preferences',preferenceModel);
