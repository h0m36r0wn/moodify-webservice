var express = require('express');
var app = express();
var logger = require('morgan');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/moodify');
var db = mongoose.connection;
var port = process.env.PORT || 3000;
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());
app.use(logger('dev'));
var routes = require('./routes');

app.listen(port, function () {
  db.once('open',function(){
    console.log('Magic happens on port ' + port);
    app.use('/api/v1/', routes.mood_route);
    app.use('/api/v1/', routes.user_route);
  })
});
