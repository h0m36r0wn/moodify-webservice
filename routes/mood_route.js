module.exports = (function(){
  var  express = require('express');
  var router = express.Router();
  var Mood =  require('../controllers/mood');

  router.route('/mood')
    .get((req, res) => {
      var mood = req.query.mood;
      if(mood != ''){
        var mood = new Mood({
          mood:mood
        });
        mood.searchPlaylist((err, playlist) => {
            res.json(playlist.body);
        })
      }
    })
  router.route('/mood/test')
    .get((req, res) => {
        var mood = new Mood({
          mood:'neutral'
        });
        mood.searchPlaylist((err, playlist) => {
            res.json({playlist:playlist.body.tracks.items});
        })
    })

  return router;

}())
