module.exports = (function(){
  var express = require('express');
  var router = express.Router();
  var Mood =  require('../controllers/mood');
  var User = require('../controllers/user');
  router.route('/user/register')
    .post((req, res) => {

        var body = req.body;

        var user = new User({
          first_name:body.first_name,
          last_name:body.last_name,
          email:body.email,
          gender:body.gender,
          password:body.passwords.password
        });

        user.registerUser((err, success) =>{
          if(err) res.json(err);
          if(success) res.json(success);
        })

    })

  router.route('/user/login')
    .post((req, res) => {
      var body = req.body;
      var user = new User({
        email:body.email,
        password:body.password
      });
      user.authUser((err, success) => {
         if(err) res.json(err);
         if(success) res.json(success);
      })
    })
  router.route('/user/test')
    .get((req, res) => {
    })
  return router;

}())
