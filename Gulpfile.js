var gulp = require('gulp');
var nodemon = require('gulp-nodemon');

// Lint Scripts

//Default task
gulp.task('default', function(){
  gulp.src('./**/*.js')
    nodemon({
        script:'app.js',
        ext:'js',
    }).on('restart',function() {
        console.log('service is restarting');
    })
})
