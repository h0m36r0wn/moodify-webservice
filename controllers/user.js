var models = require('../models');
var UserModel = models.user_model;

function User(opts){
  this.email = typeof opts != 'undefined' && typeof opts.email != 'undefined' ? opts.email : '';
  this.first_name = typeof opts != 'undefined' && typeof opts.first_name != 'undefined' ? opts.first_name : '';
  this.last_name = typeof opts != 'undefined' && typeof opts.last_name != 'undefined' ? opts.last_name : '';
  this.password = typeof opts != 'undefined' && typeof opts.password != 'undefined' ? opts.password : '';
  this.gender = typeof opts != 'undefined' && typeof opts.gender != 'undefined' ? opts.gender : '';
  this.is_new = typeof opts != 'undefined' && typeof opts.is_new != 'undefined' ? opts.is_new : '';
}


User.prototype = {

   registerUser(cb){
      var _self = this;
      var user = new UserModel();
      user.email = _self.email;
      user.first_name = _self.first_name;
      user.last_name = _self.last_name;
      user.password = _self.password;
      user.gender = _self.gender;

       user.save((err, user) => {
        if(user){
          return cb(null, {success:true,token:user});
        }else{
          return cb({success:false,title:'reg_f ailed',message:'registration failed'},null);
        }
      });

   },

   authUser(cb){
     var _self = this;
     var opts = {
       email:_self.email
     }
     UserModel.findOne(opts, (err, user) =>{    
       if(err) return cb({success:false,title:'login_failed',message:'Unknown error try again later'},null)
       if(user != null){
         if(user.password == _self.password){
           return cb(null,{success:true,token:user})
         }else{
           return cb({success:false,title:'error password',message:'the email and password you entered is not matched'},null)
         }
       }else{
         return cb({success:false,title:'user_not_found',message:'the email and password you entered is invalid'},null)

       }
     })
   }

}

module.exports = User;
