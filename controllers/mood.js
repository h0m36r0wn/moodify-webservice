var SpotifyWebApi = require('spotify-web-api-node');

var spotifyApi = new SpotifyWebApi({
  clientId : '7b8c5b5187f64fe89eb293ba7fdc25b7',
  clientSecret : 'ae71a061a8d6484c8b7907853c43e8ef'
});
function Mood(opts){
  this.mood = typeof opts != 'undefined' && typeof opts.mood != 'undefined'  ? opts.mood : '';
}

Mood.prototype = {
  searchPlaylist(cb){
    var _self = this;
    if(_self.mood != ''){
        spotifyApi.searchTracks(_self.mood).then((playlist) => {
            return cb(null, playlist);
        })
    }
  }
}

module.exports = Mood;
